/* Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
DOM - це інтерфейс, модель якого побудована з структури об'єктів у формі дерева, і з цими об'єктами можна маніпулювати за допомогою різних методів.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML та innerText - це дві різні властивості в HTML та JavaScript, які використовуються для роботи з вмістом елементів на сторінці.
innerHTML - ця властивість дає змогу отримати або встановити HTML вміст внутрішнього вмісту елемента.
innerText - ця властивість дозволяє отримати або встановити текстовий вміст внутрішнього вмісту елемента.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
document.getElementById() - Цей метод використовується для отримання посилання на елемент за його унікальним ідентифікатором (id).
document.getElementsByClassName() - Цей метод повертає колекцію елементів, що мають заданий клас.
document.getElementsByTagName() - Цей метод повертає колекцію елементів з вказаною назвою тегу.
document.querySelector() - Цей метод повертає перший елемент, який збігається з CSS-селектором.
document.querySelectorAll() - Цей метод повертає всі елементи, які збігаються з CSS-селектором.
Кращий спосіб залежить від поставленої задачі. getElementById є найшвидшим і оптимальним.

4. Яка різниця між nodeList та HTMLCollection?
Це схожі на масив колекції вузлів. 
nodeList (querySelectorAll) може зберігати всі типи вузлів (тексти, коментарі).
HTMLColection (getElement...) може зберігати лише вузли HTML елементів.

Практичні завдання
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Використайте 2 способи для пошуку елементів.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).*/


const featureFirstMethod = document.getElementsByClassName ('feature');
console.log(featureFirstMethod);

const featureSecondMethod = document.querySelectorAll ('.feature');
console.log(featureSecondMethod);

for (let el of featureFirstMethod) {
    el.style.textAlign = 'center';
}

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

const titleText = document.getElementsByTagName('h2');

for (let el of titleText) {
    el.textContent = 'Awesome feature';
};

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const featureTitle = document.getElementsByClassName('feature-title');

for ( let i=0; i<featureTitle.length; i++) {
    featureTitle[i].textContent += '!';
};